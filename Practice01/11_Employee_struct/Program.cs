﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11_Employee_struct
{
    class Program
    {
        private const int WORK_HOUR_MIN = 1;
        private const int WORK_HOUR_MAX = 24;
        private const int HOURLY_WAGE_MIN = 100;
        private const int HOURLY_WAGE_MAX = 5000;

        static void Main(string[] args)
        {
            int total = 0;
            Employee employee;
            ConsoleKeyInfo keyInfo;

            do
            {
                employee = GetEmployeeFromConsole();
                Console.WriteLine("{0}", employee);
                total += employee.CalculateWage();

                Console.Write("Van még dolgozó? (i/n) ");
                keyInfo = Console.ReadKey();
                Console.WriteLine();
            } while (char.ToLower(keyInfo.KeyChar) == 'i');

            Console.WriteLine("Összesen fizetendő bér: {0}", total);
        }

        private static int GetIntFromConsole(int min, int max)
        {
            int number;
            while (!int.TryParse(Console.ReadLine(), out number) || number < min || number > max)
            {
                Console.WriteLine("Kérem írjon egész számot ({0}-{1})!", min, max);
            }
            return number;
        }

        private static Employee GetEmployeeFromConsole()
        {
            Employee employee = new Employee();

            Console.Write("A dolgozó neve: ");
            employee.Name = Console.ReadLine();
            Console.Write("Ledolgozott órák száma: ");
            employee.WorkedHours = GetIntFromConsole(WORK_HOUR_MIN, WORK_HOUR_MAX);
            Console.Write("Órabér: ");
            employee.HourlyWage = GetIntFromConsole(HOURLY_WAGE_MIN, HOURLY_WAGE_MAX);

            return employee;
        }
    }
}
