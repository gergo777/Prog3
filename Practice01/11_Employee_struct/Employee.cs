﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11_Employee_struct
{
    struct Employee
    {
        public string Name { get; set; }
        public int WorkedHours { get; set; }
        public int HourlyWage { get; set; }

        public int CalculateWage()
        {
            return WorkedHours * HourlyWage;
        }

        public override string ToString()
        {
            return String.Format("{0} bére: {1}Ft", Name, CalculateWage());
        }
    }
}
