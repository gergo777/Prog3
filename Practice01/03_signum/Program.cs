﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_signum
{
    class Program
    {
        static void Main(string[] args)
        {
            string data;
            int number;

            do
            {
                Console.Write("Kérek egy számot: ");
                data = Console.ReadLine();
            } while (!int.TryParse(data, out number));
            if (number == 0)
            {
                Console.WriteLine("A szám 0");
            }
            else
            {
                if(number < 0)
                {
                    Console.WriteLine("A szám negatív");
                }
                else
                {
                    Console.WriteLine("A szám pozitív");
                }
            }
        }
    }
}
