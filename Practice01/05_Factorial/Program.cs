﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_Factorial
{
    class Program
    {
        private const int MAX_N = 20;

        static void Main(string[] args)
        {
            int n;

            Console.Write("Kérek egy pozitív egész számot (1-{0}) ", MAX_N);

            while (!int.TryParse(Console.ReadLine(), out n) || n < 0 || n > MAX_N)
            {
                Console.Write("Na még egyszer: ");
            }

            Console.WriteLine("{0}! = {1}", n, CalcFactorial(n));
        }

        private static int CalcFactorial(int n)
        {
            int result = 1;
            for (int i = 1; i <= n; i++)
            {
                result *= i;
            }

            return result;
        }
    }
}
