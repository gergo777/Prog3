﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Divisor
{
    class Program
    {
        private const int DIVISOR = 5;

        static void Main(string[] args)
        {
            Console.Write("Kérek egy számot: ");
            int number;
            while (!int.TryParse(Console.ReadLine(), out number) || number < 0)
            {
                Console.WriteLine("Na még egyszer!");
            }

            //Páros vizsgálat
            if (number % 2 == 0)
            {
                Console.WriteLine("A {0} szám páros", number);
            }
            else
            {
                Console.WriteLine("A {0} szám páratlan", number);
            }

            //Páros vizsgálat feltételes operátor
            Console.WriteLine(number % 2 == 0 ? "A {0} szám páros" : "A {0} szám páratlan", number);

            //Páros összevont vizsgálat
            Console.WriteLine("A {0} szám {1}páros", number, number % 2 == 0 ? "" : "nem ");

            if (number % DIVISOR == 0)
            {
                Console.WriteLine("A {0} szám osztható {1}-tel", number, DIVISOR);
            }
            else
            {
                Console.WriteLine("A {0} szám nem osztható {1}-tel", number, DIVISOR);
            }
        }
    }
}
