﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_Fibonacci
{
    class Program
    {
        const int MAX_COUNT = 30;

        static void Main(string[] args)
        {
            int count;
            Console.WriteLine("Hány számot írjak ki (1-{0})?", MAX_COUNT);
            while (!int.TryParse(Console.ReadLine(), out count) || count < 1 || count > MAX_COUNT)
            {
                Console.WriteLine("Hát, ez nem sikerült...\nKérem próbálja meg újra, és ha lehet, most (1-{0}) számot adjon meg!", MAX_COUNT);
            }

            Console.WriteLine("Ennyi lett: {0}", FibonacciCycle(count));
            Console.WriteLine("Meg ennyi: {0}", FibonacciRecursive(count));
        }

        private static int FibonacciCycle(int n)
        {
            int f = 0;
            switch (n)
            {
                case 1:
                    f = 0;
                    break;
                case 2:
                    f = 1;
                    break;
                default:
                    int fm2 = 0;
                    int fm1 = 1;
                    f = fm2 + fm1;  //ez a harmadik fibonacci szám, ezt még nem kell számolni
                    for (int i = 4; i <= n; i++)
                    {
                        fm2 = fm1;
                        fm1 = f;
                        f = fm2 + fm1;
                    }
                    break;
            }

            return f;
        }

        private static int FibonacciRecursive(int n)
        {
            if (n == 1) return 0;
            if (n == 2) return 1;

            return FibonacciRecursive(n - 1) + FibonacciRecursive(n - 2);
        }
    }
}
