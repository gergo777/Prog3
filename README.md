# Prog3
PTE MIK Programozás 3

A VisualStudio 2017 Community megoldások (solutions) tartalmazák:  
- az egyes gyakorlatokon megoldott feladatokat `PracticeXX` solution különálló projectjeiként
- a megoldások tartalmaz(hat)nak egy `Examples` project-et is, ami nem minden esetben futtatható.  
  Ez a project az órán megoldott, illetve házi feladatok rávezető példáit, példa részleteit tartalmazza.  
  A project célja, hogy ne közvetlenül az alkalmazandó megoldást mutassa be, hanem kényszerítse a hallgatót az ismeret szintetizálására egy hasonló kódrészletből.
