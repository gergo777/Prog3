﻿namespace GyumolcsBolt
{
    partial class UjRekordForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.TxtNev = new System.Windows.Forms.TextBox();
            this.TxtEgysegAr = new System.Windows.Forms.TextBox();
            this.TxtKeszlet = new System.Windows.Forms.TextBox();
            this.pctKep = new System.Windows.Forms.PictureBox();
            this.BtnHozzaAd = new System.Windows.Forms.Button();
            this.Browse = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pctKep)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Név:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 64);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Egységár:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 101);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Készlet:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // TxtNev
            // 
            this.TxtNev.Location = new System.Drawing.Point(139, 23);
            this.TxtNev.Margin = new System.Windows.Forms.Padding(4);
            this.TxtNev.Name = "TxtNev";
            this.TxtNev.Size = new System.Drawing.Size(132, 22);
            this.TxtNev.TabIndex = 4;
            // 
            // TxtEgysegAr
            // 
            this.TxtEgysegAr.Location = new System.Drawing.Point(139, 60);
            this.TxtEgysegAr.Margin = new System.Windows.Forms.Padding(4);
            this.TxtEgysegAr.Name = "TxtEgysegAr";
            this.TxtEgysegAr.Size = new System.Drawing.Size(132, 22);
            this.TxtEgysegAr.TabIndex = 5;
            // 
            // TxtKeszlet
            // 
            this.TxtKeszlet.Location = new System.Drawing.Point(139, 97);
            this.TxtKeszlet.Margin = new System.Windows.Forms.Padding(4);
            this.TxtKeszlet.Name = "TxtKeszlet";
            this.TxtKeszlet.Size = new System.Drawing.Size(132, 22);
            this.TxtKeszlet.TabIndex = 6;
            // 
            // pctKep
            // 
            this.pctKep.Location = new System.Drawing.Point(140, 139);
            this.pctKep.Margin = new System.Windows.Forms.Padding(4);
            this.pctKep.Name = "pctKep";
            this.pctKep.Size = new System.Drawing.Size(107, 98);
            this.pctKep.TabIndex = 7;
            this.pctKep.TabStop = false;
            // 
            // BtnHozzaAd
            // 
            this.BtnHozzaAd.Location = new System.Drawing.Point(96, 274);
            this.BtnHozzaAd.Margin = new System.Windows.Forms.Padding(4);
            this.BtnHozzaAd.Name = "BtnHozzaAd";
            this.BtnHozzaAd.Size = new System.Drawing.Size(156, 28);
            this.BtnHozzaAd.TabIndex = 8;
            this.BtnHozzaAd.Text = "Rekordot beszúr";
            this.BtnHozzaAd.UseVisualStyleBackColor = true;
            this.BtnHozzaAd.Click += new System.EventHandler(this.BtnHozzaAd_Click);
            // 
            // Browse
            // 
            this.Browse.Location = new System.Drawing.Point(255, 174);
            this.Browse.Margin = new System.Windows.Forms.Padding(4);
            this.Browse.Name = "Browse";
            this.Browse.Size = new System.Drawing.Size(28, 28);
            this.Browse.TabIndex = 9;
            this.Browse.Text = "...";
            this.Browse.UseVisualStyleBackColor = true;
            this.Browse.Click += new System.EventHandler(this.Browse_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(280, 62);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Ft";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(280, 98);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Kg";
            // 
            // UjRekordForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LavenderBlush;
            this.ClientSize = new System.Drawing.Size(345, 321);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Browse);
            this.Controls.Add(this.BtnHozzaAd);
            this.Controls.Add(this.pctKep);
            this.Controls.Add(this.TxtKeszlet);
            this.Controls.Add(this.TxtEgysegAr);
            this.Controls.Add(this.TxtNev);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UjRekordForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Új gyümölcs";
            this.Load += new System.EventHandler(this.UjRekordForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pctKep)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox TxtNev;
        private System.Windows.Forms.TextBox TxtEgysegAr;
        private System.Windows.Forms.TextBox TxtKeszlet;
        private System.Windows.Forms.PictureBox pctKep;
        private System.Windows.Forms.Button BtnHozzaAd;
        private System.Windows.Forms.Button Browse;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}