﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GyumolcsBolt
{
    public partial class Gyumolcs
    {
        public override string ToString()
        {
            return Nev.PadRight(16)+Keszlet.ToString("0.00")+" kg";
        }
    }
}
