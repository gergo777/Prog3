﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace GyumolcsBolt
{
    public partial class ModositasForm : Form
    {
        public ModositasForm()
        {
            InitializeComponent();
        }

        private void ModositasForm_Load(object sender, EventArgs e)
        {
            AdatBetoltes();
        }

        private void AdatBetoltes()
        {
            using (var context = new GyumolcsDBContext()) {
                var gyumolcsLista = from Gyumolcs gyumolcs in context.Gyumolcsok
                                    select gyumolcs;
                lstGyumolcsok.DataSource = gyumolcsLista.ToList();
            }
        }

        private void lstGyumolcsok_SelectedIndexChanged(object sender, EventArgs e)
        {
            KivalasztottatMutat();
        }

        private void KivalasztottatMutat()
        {
            Gyumolcs gyumolcs = lstGyumolcsok.SelectedItem as Gyumolcs;
            if (gyumolcs==null)
            {
                return;
            }
            TxtKod.Text = gyumolcs.GyumolcsID.ToString();
            TxtNev.Text = gyumolcs.Nev;
            TxtKeszlet.Text =String.Format("{0:0.00} ",gyumolcs.Keszlet);
            TxtEgysegAr.Text = gyumolcs.EgysegAr.ToString();
            pctKep.Image = Image.FromFile(gyumolcs.KepFajl);
            pctKep.SizeMode = PictureBoxSizeMode.StretchImage;
        }


        private void BtnModosit_Click(object sender, EventArgs e)
        {
            int kod = (lstGyumolcsok.SelectedItem as Gyumolcs).GyumolcsID;
           
            using (var context = new GyumolcsDBContext()) {
                var gyumolcs = context.Gyumolcsok.Find(kod);

                string ar = TxtEgysegAr.Text;
                gyumolcs.EgysegAr = int.Parse(ar);

                string keszlet =TxtKeszlet.Text;
                gyumolcs.Keszlet = double.Parse(keszlet);
                context.SaveChanges();
            }
            AdatBetoltes();
           
        }

        private void BtnMegsem_Click(object sender, EventArgs e)
        {
            KivalasztottatMutat();
        }

        private void BtnTorol_Click(object sender, EventArgs e)
        {
            try
            {
                using (var context = new GyumolcsDBContext())
                {
                    Gyumolcs gyumolcs = new Gyumolcs { GyumolcsID = (lstGyumolcsok.SelectedItem as Gyumolcs).GyumolcsID };

                    context.Entry(gyumolcs).State = System.Data.Entity.EntityState.Deleted;
                    context.SaveChanges();
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            AdatBetoltes();
        }

        private void BtnUj_Click(object sender, EventArgs e)
        {
            UjRekordForm frmUj = new UjRekordForm();
            DialogResult result;

            do {
                frmUj.Edited = new Gyumolcs();
                result = frmUj.ShowDialog();
                if(result == DialogResult.OK)
                {
                    using (var context = new GyumolcsDBContext())
                    {
                        context.Gyumolcsok.Add(frmUj.Edited);
                        context.SaveChanges();
                    }
                }
            } while (result == DialogResult.OK);
            AdatBetoltes();
        }
    }
}
