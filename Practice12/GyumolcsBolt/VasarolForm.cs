﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace GyumolcsBolt
{
    public partial class VasarolForm : Form
    {
        public VasarolForm()
        {
            InitializeComponent();
        }

        private List<Gyumolcs> gyumolcsok = new List<Gyumolcs>();
        private List<PictureBox> imagesBoxes = new List<PictureBox>();

        int szelesseg = 65, magassag = 65;
        int indexValaszt=-1;

        private void VasarolForm_Load(object sender, EventArgs e)
        {
            Adatbeolvasas();
            for (int i = 0; i < gyumolcsok.Count; i++)
            {
                PictureBox picture = new PictureBox();
                picture.Name = gyumolcsok[i].Nev;
                picture.Image = Image.FromFile(gyumolcsok[i].KepFajl);
                picture.Size = new Size(szelesseg, magassag);
                picture.SizeMode = PictureBoxSizeMode.StretchImage;
                picture.Click += new System.EventHandler(this.picture_Click);

                imagesBoxes.Add(picture);
                flowLayoutPanel1.Controls.Add(picture);
                BtnMegvesz.Enabled = false;
            }
        }

        private void picture_Click(object sender, EventArgs e)
        {
            string nev = (sender as PictureBox).Name;
            int index = imagesBoxes.IndexOf(sender as PictureBox);
            indexValaszt = index;
            
            BtnMeres.Focus();
        }

        private void BtnMeres_Click(object sender, EventArgs e)
        {
            if (indexValaszt < 0)
            {
                MessageBox.Show("Mérés előtt válasszon képet!");
                return;
            }
            int fizet = 0;
            double mennyiseg;
            Random rand = new Random();
            mennyiseg = rand.NextDouble() * 10;
            TxtMennyiseg.Text =String.Format("{0:0.00}",mennyiseg);
            fizet = (int)(mennyiseg * gyumolcsok[indexValaszt].EgysegAr);
            TxtKijelzes.Text = mennyiseg.ToString("0.00") + " (kg) "+gyumolcsok[indexValaszt].Nev+
                " * " + gyumolcsok[indexValaszt].EgysegAr + " Ft = " + fizet + " Ft";
            TxtAr.Text = fizet + " Ft";
            BtnMegvesz.Enabled = true;
        }

        private void BtnMegvesz_Click(object sender, EventArgs e)
        {
            using (var context = new GyumolcsDBContext())
            {
                var gyumolcs = context.Gyumolcsok.Find(gyumolcsok[indexValaszt].GyumolcsID);
                MessageBox.Show(indexValaszt + "  " + gyumolcs);
                gyumolcs.Keszlet -= double.Parse(TxtMennyiseg.Text);
                context.SaveChanges();
            }
            MessageBox.Show("A vásárolt mennyiséget \n levontuk a készletből.");
            TxtAr.Clear();
            TxtKijelzes.Clear();
            TxtMennyiseg.Clear();
            BtnMegvesz.Enabled = false;
        }

        private void Adatbeolvasas()
        {
            using (var context=new GyumolcsDBContext())
            {
                var gyumolcstabla = from Gyumolcs gyumolcs in context.Gyumolcsok
                                    select gyumolcs;
                foreach (Gyumolcs item in gyumolcstabla)
                {
                    gyumolcsok.Add(item);
                }
            }
              
        }
    }
}
