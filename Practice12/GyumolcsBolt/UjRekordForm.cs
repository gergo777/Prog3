﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace GyumolcsBolt
{
    public partial class UjRekordForm : Form
    {
        public Gyumolcs Edited { get; set; }

        public UjRekordForm()
        {
            InitializeComponent();

            Edited = new Gyumolcs();

            openFileDialog1.InitialDirectory = Environment.CurrentDirectory;
        }

        private void UjRekordForm_Load(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void BtnHozzaAd_Click(object sender, EventArgs e)
        {
            try
            {
                Edited.Nev = TxtNev.Text;
                Edited.EgysegAr = int.Parse(TxtEgysegAr.Text);
                Edited.Keszlet = double.Parse(TxtKeszlet.Text);

                DialogResult = DialogResult.OK;
                Close();
            }
            catch (FormatException ex) {
                MessageBox.Show("Hibás adat: " + ex.Message, "Hiba");
                TxtEgysegAr.Focus();
            }
        }

        private void Browse_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK) 
            {
                Edited.KepFajl = Path.GetFileName(openFileDialog1.FileName);
                pctKep.Image = Image.FromFile(openFileDialog1.FileName);
                pctKep.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }
    }
}
