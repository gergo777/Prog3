﻿using System.Collections.Generic;
using System.IO;

namespace GyumolcsBolt
{
    public class GyumolcsDBFeltolt
    {
        public static void Betolt()
        {
            using (GyumolcsDBContext context = new GyumolcsDBContext())
            {
                foreach (var item in BeolvasGyumolcs())
                {
                    context.Gyumolcsok.Add(item);
                }
                context.SaveChanges();
            }
        }

        private static List<Gyumolcs> BeolvasGyumolcs()
        {
            var gyumolcsok = new List<Gyumolcs>();
            Gyumolcs gyumolcs;
            string[] adatSor;
            using (StreamReader reader = new StreamReader("gyumolcsok.txt"))
            {
                while (!reader.EndOfStream)
                {
                    adatSor = reader.ReadLine().Split(';');
                    gyumolcs = new Gyumolcs();
                   
                    gyumolcs.Nev = adatSor[0];
                    gyumolcs.EgysegAr = int.Parse(adatSor[1]);
                    gyumolcs.Keszlet = double.Parse(adatSor[2]);
                    gyumolcs.KepFajl = adatSor[3];
                    //az objektumot a listához adjuk
                    gyumolcsok.Add(gyumolcs);
                }
            }
            return gyumolcsok;
        }
    }
}


