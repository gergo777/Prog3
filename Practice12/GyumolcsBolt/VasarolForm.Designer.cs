﻿namespace GyumolcsBolt
{
    partial class VasarolForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TxtMennyiseg = new System.Windows.Forms.TextBox();
            this.BtnMeres = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtAr = new System.Windows.Forms.TextBox();
            this.BtnMegvesz = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtKijelzes = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(25, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mennyiség (kg):";
            // 
            // TxtMennyiseg
            // 
            this.TxtMennyiseg.Location = new System.Drawing.Point(114, 16);
            this.TxtMennyiseg.Name = "TxtMennyiseg";
            this.TxtMennyiseg.ReadOnly = true;
            this.TxtMennyiseg.Size = new System.Drawing.Size(100, 20);
            this.TxtMennyiseg.TabIndex = 1;
            this.TxtMennyiseg.TabStop = false;
            // 
            // BtnMeres
            // 
            this.BtnMeres.Location = new System.Drawing.Point(241, 12);
            this.BtnMeres.Name = "BtnMeres";
            this.BtnMeres.Size = new System.Drawing.Size(75, 23);
            this.BtnMeres.TabIndex = 2;
            this.BtnMeres.Text = "Mérés";
            this.BtnMeres.UseVisualStyleBackColor = true;
            this.BtnMeres.Click += new System.EventHandler(this.BtnMeres_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(28, 52);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(329, 288);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 398);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Ár:";
            // 
            // TxtAr
            // 
            this.TxtAr.Location = new System.Drawing.Point(114, 398);
            this.TxtAr.Name = "TxtAr";
            this.TxtAr.ReadOnly = true;
            this.TxtAr.Size = new System.Drawing.Size(100, 20);
            this.TxtAr.TabIndex = 5;
            this.TxtAr.TabStop = false;
            // 
            // BtnMegvesz
            // 
            this.BtnMegvesz.Location = new System.Drawing.Point(254, 395);
            this.BtnMegvesz.Name = "BtnMegvesz";
            this.BtnMegvesz.Size = new System.Drawing.Size(75, 23);
            this.BtnMegvesz.TabIndex = 6;
            this.BtnMegvesz.Text = "Megveszi";
            this.BtnMegvesz.UseVisualStyleBackColor = true;
            this.BtnMegvesz.Click += new System.EventHandler(this.BtnMegvesz_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(57, 367);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Kijelző:";
            // 
            // TxtKijelzes
            // 
            this.TxtKijelzes.Location = new System.Drawing.Point(114, 364);
            this.TxtKijelzes.Name = "TxtKijelzes";
            this.TxtKijelzes.ReadOnly = true;
            this.TxtKijelzes.Size = new System.Drawing.Size(215, 20);
            this.TxtKijelzes.TabIndex = 8;
            this.TxtKijelzes.TabStop = false;
            // 
            // VasarolForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PowderBlue;
            this.ClientSize = new System.Drawing.Size(388, 441);
            this.Controls.Add(this.TxtKijelzes);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BtnMegvesz);
            this.Controls.Add(this.TxtAr);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.BtnMeres);
            this.Controls.Add(this.TxtMennyiseg);
            this.Controls.Add(this.label1);
            this.Name = "VasarolForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gyümölcs vásárlás";
            this.Load += new System.EventHandler(this.VasarolForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtMennyiseg;
        private System.Windows.Forms.Button BtnMeres;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtAr;
        private System.Windows.Forms.Button BtnMegvesz;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtKijelzes;
    }
}

