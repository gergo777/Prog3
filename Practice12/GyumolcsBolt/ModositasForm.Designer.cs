﻿namespace GyumolcsBolt
{
    partial class ModositasForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstGyumolcsok = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtKod = new System.Windows.Forms.TextBox();
            this.TxtNev = new System.Windows.Forms.TextBox();
            this.TxtEgysegAr = new System.Windows.Forms.TextBox();
            this.TxtKeszlet = new System.Windows.Forms.TextBox();
            this.pctKep = new System.Windows.Forms.PictureBox();
            this.BtnUj = new System.Windows.Forms.Button();
            this.BtnModosit = new System.Windows.Forms.Button();
            this.BtnMegsem = new System.Windows.Forms.Button();
            this.BtnTorol = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pctKep)).BeginInit();
            this.SuspendLayout();
            // 
            // lstGyumolcsok
            // 
            this.lstGyumolcsok.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lstGyumolcsok.FormattingEnabled = true;
            this.lstGyumolcsok.ItemHeight = 19;
            this.lstGyumolcsok.Location = new System.Drawing.Point(43, 47);
            this.lstGyumolcsok.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lstGyumolcsok.Name = "lstGyumolcsok";
            this.lstGyumolcsok.Size = new System.Drawing.Size(315, 270);
            this.lstGyumolcsok.TabIndex = 0;
            this.lstGyumolcsok.SelectedIndexChanged += new System.EventHandler(this.lstGyumolcsok_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(372, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Gyümölcs kód:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(435, 97);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Név:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(403, 148);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Egységár:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(416, 198);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Készlet:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(436, 249);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Kép:";
            // 
            // TxtKod
            // 
            this.TxtKod.Location = new System.Drawing.Point(501, 43);
            this.TxtKod.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtKod.Name = "TxtKod";
            this.TxtKod.ReadOnly = true;
            this.TxtKod.Size = new System.Drawing.Size(132, 22);
            this.TxtKod.TabIndex = 6;
            // 
            // TxtNev
            // 
            this.TxtNev.Location = new System.Drawing.Point(501, 92);
            this.TxtNev.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtNev.Name = "TxtNev";
            this.TxtNev.ReadOnly = true;
            this.TxtNev.Size = new System.Drawing.Size(132, 22);
            this.TxtNev.TabIndex = 7;
            // 
            // TxtEgysegAr
            // 
            this.TxtEgysegAr.Location = new System.Drawing.Point(501, 142);
            this.TxtEgysegAr.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtEgysegAr.Name = "TxtEgysegAr";
            this.TxtEgysegAr.Size = new System.Drawing.Size(132, 22);
            this.TxtEgysegAr.TabIndex = 8;
            // 
            // TxtKeszlet
            // 
            this.TxtKeszlet.Location = new System.Drawing.Point(501, 191);
            this.TxtKeszlet.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtKeszlet.Name = "TxtKeszlet";
            this.TxtKeszlet.Size = new System.Drawing.Size(129, 22);
            this.TxtKeszlet.TabIndex = 9;
            // 
            // pctKep
            // 
            this.pctKep.Location = new System.Drawing.Point(501, 249);
            this.pctKep.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pctKep.Name = "pctKep";
            this.pctKep.Size = new System.Drawing.Size(107, 98);
            this.pctKep.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctKep.TabIndex = 10;
            this.pctKep.TabStop = false;
            // 
            // BtnUj
            // 
            this.BtnUj.Location = new System.Drawing.Point(87, 377);
            this.BtnUj.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnUj.Name = "BtnUj";
            this.BtnUj.Size = new System.Drawing.Size(100, 28);
            this.BtnUj.TabIndex = 11;
            this.BtnUj.Text = "Új";
            this.BtnUj.UseVisualStyleBackColor = true;
            this.BtnUj.Click += new System.EventHandler(this.BtnUj_Click);
            // 
            // BtnModosit
            // 
            this.BtnModosit.Location = new System.Drawing.Point(227, 377);
            this.BtnModosit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnModosit.Name = "BtnModosit";
            this.BtnModosit.Size = new System.Drawing.Size(100, 28);
            this.BtnModosit.TabIndex = 12;
            this.BtnModosit.Text = "Módosít";
            this.BtnModosit.UseVisualStyleBackColor = true;
            this.BtnModosit.Click += new System.EventHandler(this.BtnModosit_Click);
            // 
            // BtnMegsem
            // 
            this.BtnMegsem.Location = new System.Drawing.Point(507, 377);
            this.BtnMegsem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnMegsem.Name = "BtnMegsem";
            this.BtnMegsem.Size = new System.Drawing.Size(100, 28);
            this.BtnMegsem.TabIndex = 13;
            this.BtnMegsem.Text = "Mégsem";
            this.BtnMegsem.UseVisualStyleBackColor = true;
            this.BtnMegsem.Click += new System.EventHandler(this.BtnMegsem_Click);
            // 
            // BtnTorol
            // 
            this.BtnTorol.Location = new System.Drawing.Point(367, 377);
            this.BtnTorol.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnTorol.Name = "BtnTorol";
            this.BtnTorol.Size = new System.Drawing.Size(100, 28);
            this.BtnTorol.TabIndex = 14;
            this.BtnTorol.Text = "Töröl";
            this.BtnTorol.UseVisualStyleBackColor = true;
            this.BtnTorol.Click += new System.EventHandler(this.BtnTorol_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(61, 21);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "Név";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(209, 22);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "Készlet";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(644, 148);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(20, 17);
            this.label8.TabIndex = 17;
            this.label8.Text = "Ft";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(641, 198);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 17);
            this.label9.TabIndex = 18;
            this.label9.Text = "Kg";
            // 
            // ModositasForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FloralWhite;
            this.ClientSize = new System.Drawing.Size(720, 434);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.BtnTorol);
            this.Controls.Add(this.BtnMegsem);
            this.Controls.Add(this.BtnModosit);
            this.Controls.Add(this.BtnUj);
            this.Controls.Add(this.pctKep);
            this.Controls.Add(this.TxtKeszlet);
            this.Controls.Add(this.TxtEgysegAr);
            this.Controls.Add(this.TxtNev);
            this.Controls.Add(this.TxtKod);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstGyumolcsok);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ModositasForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Adatok módosítása";
            this.Load += new System.EventHandler(this.ModositasForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pctKep)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstGyumolcsok;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtKod;
        private System.Windows.Forms.TextBox TxtNev;
        private System.Windows.Forms.TextBox TxtEgysegAr;
        private System.Windows.Forms.TextBox TxtKeszlet;
        private System.Windows.Forms.PictureBox pctKep;
        private System.Windows.Forms.Button BtnUj;
        private System.Windows.Forms.Button BtnModosit;
        private System.Windows.Forms.Button BtnMegsem;
        private System.Windows.Forms.Button BtnTorol;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}