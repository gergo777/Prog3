﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GyumolcsBolt
{
    public partial class Gyumolcs
    {
        public int GyumolcsID { get; set; }
        [Required,StringLength(15)]
        public string Nev { get; set; }
        public int EgysegAr { get; set; }
        public double Keszlet { get; set; }
        public string KepFajl { get; set; }

        public Gyumolcs()
        {

        }

        public Gyumolcs(string nev, int ar, double keszlet, string kepFajl)
        {
            Nev = nev;
            EgysegAr = ar;
            Keszlet = keszlet;
            KepFajl = kepFajl;
        }
    }
}
