﻿using System.Data.Entity;

namespace GyumolcsBolt
{
   public class GyumolcsDBContext:DbContext
    {
        public GyumolcsDBContext() : base("name=GyumolcsDB") {

            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<GyumolcsDBContext>());
        }

        public DbSet<Gyumolcs> Gyumolcsok{ get; set; }

        public bool DbExists {
            get
            {
                return Database.Exists();
            }
        }
    }
}
