﻿namespace GyumolcsBolt
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.VasarlasMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adminisztrációToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CreateDbMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AdatModositasMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kilepesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.VasarlasMenuItem,
            this.adminisztrációToolStripMenuItem,
            this.kilepesMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(579, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // VasarlasMenuItem
            // 
            this.VasarlasMenuItem.Name = "VasarlasMenuItem";
            this.VasarlasMenuItem.Size = new System.Drawing.Size(74, 24);
            this.VasarlasMenuItem.Text = "Vásárlás";
            this.VasarlasMenuItem.Click += new System.EventHandler(this.VasarlasMenuItem_Click);
            // 
            // adminisztrációToolStripMenuItem
            // 
            this.adminisztrációToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CreateDbMenuItem,
            this.AdatModositasMenuItem});
            this.adminisztrációToolStripMenuItem.Name = "adminisztrációToolStripMenuItem";
            this.adminisztrációToolStripMenuItem.Size = new System.Drawing.Size(120, 24);
            this.adminisztrációToolStripMenuItem.Text = "Adminisztráció";
            // 
            // CreateDbMenuItem
            // 
            this.CreateDbMenuItem.Name = "CreateDbMenuItem";
            this.CreateDbMenuItem.Size = new System.Drawing.Size(230, 26);
            this.CreateDbMenuItem.Text = "Adatbázis létrehozása";
            this.CreateDbMenuItem.Click += new System.EventHandler(this.CreateDbMenuItem_Click);
            // 
            // AdatModositasMenuItem
            // 
            this.AdatModositasMenuItem.Name = "AdatModositasMenuItem";
            this.AdatModositasMenuItem.Size = new System.Drawing.Size(230, 26);
            this.AdatModositasMenuItem.Text = "Adatok módosítása";
            this.AdatModositasMenuItem.Click += new System.EventHandler(this.AdatModositasMenuItem_Click);
            // 
            // kilepesMenuItem
            // 
            this.kilepesMenuItem.Name = "kilepesMenuItem";
            this.kilepesMenuItem.Size = new System.Drawing.Size(69, 24);
            this.kilepesMenuItem.Text = "Kilépés";
            this.kilepesMenuItem.Click += new System.EventHandler(this.kilepesMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::GyumolcsBolt.Properties.Resources.gyumolcsBolt;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(579, 393);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Friss és Egészséges";
            this.Load += new System.EventHandler(this.StartForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem VasarlasMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adminisztrációToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CreateDbMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AdatModositasMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kilepesMenuItem;
    }
}

