﻿using System;
using System.Windows.Forms;

namespace GyumolcsBolt
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void CreateDbMenuItem_Click(object sender, EventArgs e)
        {
            GyumolcsDBFeltolt.Betolt();
            UpdateMenuItemState();
        }

        private void AdatModositasMenuItem_Click(object sender, EventArgs e)
        {
            ModositasForm frmModosit = new ModositasForm();
            frmModosit.ShowDialog();
        }

        private void VasarlasMenuItem_Click(object sender, EventArgs e)
        {
            VasarolForm frmVasarol = new VasarolForm();
            frmVasarol.Show();
        }

        private void kilepesMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void StartForm_Load(object sender, EventArgs e)
        {
            UpdateMenuItemState();
        }

        private void UpdateMenuItemState()
        {
            using (var context = new GyumolcsDBContext())
            {
                CreateDbMenuItem.Enabled = !context.DbExists;
                AdatModositasMenuItem.Enabled = !CreateDbMenuItem.Enabled;
            }
        }
    }
}
