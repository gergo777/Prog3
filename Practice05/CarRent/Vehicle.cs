﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRent
{
    abstract class Vehicle
    {
        public enum Statuses
        {
            Available,
            Rented,
            InTransport
        }

        public static int DefaultFare { get; set; }  = 20000;

        public string Licence { get; private set; }
        public int ProdYear { get; private set; }
        public string Id { get; private set; }

        public int Kms { get; set; }
        public Statuses Status { get; set; }
        public bool IsAvailable
        {
            get
            {
                return Status == Statuses.Available;
            }
        }

        virtual public int Fare
        {
            get
            {
                return DefaultFare;
            }
        }

        public Vehicle(string licence, int prodYear, string id)
        {
            Licence = licence;
            ProdYear = prodYear;
            Id = id;
            Status = Statuses.Available;
        }
    }
}
