﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRent
{
    class VehicleCatalog
    {
        List<Vehicle> vehicles = new List<Vehicle>();

        public VehicleCatalog(string fileName)
        {
            Initialize(fileName);
        }

        private void Initialize(string fileName)
        {
            vehicles.Clear();

            StreamReader reader = new StreamReader(fileName);
            string type;
            while (!reader.EndOfStream)
            {
                type = reader.ReadLine();
                if(type == "busz")
                {
                    vehicles.Add(
                        new Bus(
                            reader.ReadLine(),
                            Convert.ToInt32(reader.ReadLine()),
                            String.Format("B{0}", vehicles.Count + 1),
                            Convert.ToInt32(reader.ReadLine())
                        )
                    );
                }
                else
                {
                    vehicles.Add(
                        new Truck(
                            reader.ReadLine(),
                            Convert.ToInt32(reader.ReadLine()),
                            String.Format("T{0}", vehicles.Count + 1),
                            Convert.ToDouble(reader.ReadLine())
                        )
                    );
                }
            }
            reader.Close();
        }

        public void PrintList()
        {
            foreach(Vehicle v in vehicles)
            {
                Console.WriteLine(v);
            }
        }

        #region Find

        public Vehicle Find(string licence)
        {
            foreach(Vehicle v in vehicles)
            {
                if(v.Licence == licence)
                {
                    return v;
                }
            }

            return null;
        }

        public List<string> FindBusWithSeats(int requiredSeatCount)
        {
            List<string> matches = new List<string>();
            foreach (Vehicle v in vehicles) {
                Bus bus = v as Bus;
                if (v != null)
                {
                    if(bus.SeatCount >= requiredSeatCount)
                    {
                        matches.Add(bus.Licence);
                    }
                }
            }

            return matches;
        }

        public List<string> FindTruckWithWeight(double requiredWeight)
        {
            List<string> matches = new List<string>();
            foreach (Vehicle v in vehicles)
            {
                Truck truck = v as Truck;
                if (v != null)
                {
                    if (truck.MaxWeight >= requiredWeight)
                    {
                        matches.Add(truck.Licence);
                    }
                }
            }

            return matches;
        }

        #endregion

        #region Rent

        public bool Rent(string licence)
        {
            Vehicle found = Find(licence);
            if (found != null)
            {
                found.Status = Vehicle.Statuses.Rented;
            }
            return found != null;
        }

        public bool Transport(string licence)
        {
            Vehicle found = Find(licence);
            if (found != null)
            {
                found.Status = Vehicle.Statuses.InTransport;
            }
            return found != null;
        }

        public bool Return(string licence)
        {
            Vehicle found = Find(licence);
            if (found != null)
            {
                found.Status = Vehicle.Statuses.Available;
            }
            return found != null;
        }

        #endregion

        #region Shop

        public void Buy(Vehicle newVehicle)
        {
            vehicles.Add(newVehicle);
        }

        public void Scrap(string licence)
        {
            vehicles.Remove(Find(licence));
        }

        #endregion
    }
}
