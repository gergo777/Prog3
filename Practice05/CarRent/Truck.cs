﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRent
{
    class Truck : Vehicle
    {
        public static double TonPrice { get; set; }  = 2000;

        public double MaxWeight { get; private set; }

        public Truck(string licence, int prodYear, string id, double maxWeight)
            : base(licence, prodYear, id)
        {
            MaxWeight = maxWeight;
        }

        override public int Fare
        {
            get
            {
                return base.Fare + (int)(MaxWeight * TonPrice);
            }
        }

        public override string ToString()
        {
            return String.Format("{0} rendszámú, {1} rendszámú teherautó {2} tonnát szállíthat, bérleti díja {3}Ft {4}", 
                Id, Licence, MaxWeight, Fare, IsAvailable ? "bérelhető" : "nem bérelhető");
        }
    }
}
