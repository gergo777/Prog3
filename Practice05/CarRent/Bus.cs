﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRent
{
    class Bus : Vehicle
    {
        public static int SeatPrice { get; set; }  = 20;

        public int SeatCount { get; private set; }

        public Bus(string licence, int prodYear, string id, int seatCount) 
            : base(licence, prodYear, id)
        {
            SeatCount = seatCount;
        }

        override public int Fare
        {
            get
            {
                return base.Fare + SeatCount * SeatPrice;
            }
        }

        public override string ToString()
        {
            return String.Format("{0} azonosítójú, {1} rendszámú busz {2} ülőhelyes, bérleti díja {3}Ft - {4}", 
                Id, Licence, SeatCount, Fare, IsAvailable ? "bérelhető" : "nem bérelhető");
        }
    }
}
