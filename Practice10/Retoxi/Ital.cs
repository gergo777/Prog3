﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Retoxi
{
    public class Ital
    {
        public string ItalNev { get; private set; }
        public int EgysegAr { get; set; }

        public int Rendeles { get; set; }
        public int FizetettRendeles { get; set; }

        public int Fizetendo
        {
            get
            {
                return Rendeles * EgysegAr;
            }
        }

        public Ital(string italNev, int egysegAr)
        {
            this.ItalNev = italNev;
            this.EgysegAr = egysegAr;
            Rendeles = 0;
            FizetettRendeles = 0;
        }

        public string Arlistaba()
        {
            return ItalNev + " (" + EgysegAr + " Ft)";
        }

        public override string ToString()
        {
            return Rendeles.ToString().PadLeft(4) + " " + ItalNev.PadRight(33) +
                   Fizetendo.ToString().PadLeft(10) + " Ft";
        }

    }
}
