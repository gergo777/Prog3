﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Retoxi
{
    public partial class MainForm : Form
    {
        private List<Ital> italok = new List<Ital>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ReadFile())
            {
                itallapToolStripMenuItem.Enabled = true;
                saveToolStripMenuItem.Enabled = true;

                ShowItallap();
            }
        }

        private bool ReadFile()
        {
            bool retVal = false;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamReader reader = null;
                try
                {
                    reader = new StreamReader(openFileDialog1.FileName);
                    string[] dataItems;
                    while (!reader.EndOfStream)
                    {
                        dataItems = reader.ReadLine().Split(';');
                        italok.Add(
                            new Ital(
                                dataItems[0],
                                int.Parse(dataItems[1])
                            )
                        );
                    }
                    retVal = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                        String.Format("Hiba történt a fájl olvasása közben! {0}", ex.Message),
                        "Figyeleme!"
                    );
                }
                finally
                {
                    if (reader != null) reader.Close();
                }
            }

            return retVal;
        }

        private void itallapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowItallap();
        }

        private void ShowItallap()
        {
            ItallapForm itallapForm = new ItallapForm(italok);
            itallapForm.ShowDialog();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Készült a Programozás 3 tantárgy 10. gyakorlatán (2018)", "Névjegy");
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HelpForm helpForm = new HelpForm();

            helpForm.ShowDialog();
        }

        private void galeryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GalleryForm galleryForm = new GalleryForm();
            galleryForm.ShowDialog();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamWriter writer = new StreamWriter(saveFileDialog1.FileName);
                writer.WriteLine("Eladott italadagok:");
                foreach(Ital ital in italok)
                {
                    writer.WriteLine(ital.ToString());
                }
                writer.Close();
            }
        }
    }
}
