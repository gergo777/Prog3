﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PizzaOrder
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.CenterToScreen();

            SetControlsVisibility(false);
        }

        private int meretKicsi = 32, meretNagy = 45;
        private const int yDiff = 41;

        private List<Pizza> pizzak = new List<Pizza>();
        List<PizzaUI> pizzaControls = new List<PizzaUI>();

        /// <summary>
        /// A megadott olvasócsatornáról soronként beolvassa az adatokat,
        /// és a Feldolgoz() metódussal feldolgoztat egy-egy sort.
        /// 
        /// </summary>
        /// <param name="olvasoCsatorna"></param>
        private void AdatBeolvasas(StreamReader olvasoCsatorna)
        {
            string adat;

            while (!olvasoCsatorna.EndOfStream)
            {
                adat = olvasoCsatorna.ReadLine();
                Feldolgoz(adat);
            }

        }

        private void btnTorol_Click(object sender, EventArgs e)
        {
            ResetUI();
        }

        private void ResetUI()
        {
            foreach (PizzaUI ctrl in pizzaControls)
                ctrl.Clear();

            txtFizetendo.Clear();
        }

        private void btnSzamol_Click(object sender, EventArgs e)
        {
            try
            {
                int osszeg = 0;

                foreach (PizzaUI ctrl in pizzaControls)
                {
                    osszeg += ctrl.GetTotalPrice();
                }

                txtFizetendo.Text = osszeg.ToString();
            }
            catch (FormatException)
            {
                MessageBox.Show("Hibás mennyiség!", "Figyelem!");
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("A mennyiség legyen pozitív!", "Figyelem!");
            }
        }

        /// <summary>
        /// 
        /// A beolvasott sort felvágja a ';' mentén, és az így kapott
        /// adatok alapján létrehoz egy Pizza típusú példányt, és 
        /// hozzáadja a pizzák listájához.
        /// 
        /// </summary>
        /// <param name="adat"></param>
        /// 
        private void Feldolgoz(string adat)
        {
            string[] words = adat.Split(';');

            pizzak.Add(
                new Pizza(
                    words[0],
                    int.Parse(words[1]),
                    int.Parse(words[2])
                )
            );
        }

        private void btnAdatBe_Click(object sender, EventArgs e)
        {
            LoadFile();
            InitUI();
            ResetUI();
            SetControlsVisibility(true);
        }

        private void LoadFile()
        {
            try
            {
                pizzak.Clear();
                DialogResult selected = openFileDialog1.ShowDialog();
                if (selected == DialogResult.OK)
                {
                    StreamReader reader = new StreamReader(
                        openFileDialog1.FileName
                    );

                    AdatBeolvasas(reader);
                    reader.Close();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Fájl olvasási hiba!", "Figyelem!");
            }
        }

        private void InitUI()
        {
            lblKicsi.Text = meretKicsi.ToString() + " cm";
            lblNagy.Text = meretNagy.ToString() + " cm";

            int rowCount = 0;
            DynamicContentContainer.Controls.Clear();
            foreach (Pizza pizza in pizzak)
            {
                pizzaControls.Add(
                    new PizzaUI(
                        DynamicContentContainer, 
                        rowCount * yDiff, 
                        pizza
                    )
                );

                rowCount++;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(MessageBox.Show("Biztossan ki szeretne lépni?", "Figyelem!", MessageBoxButtons.YesNo) != DialogResult.Yes)
            {
                e.Cancel = true;
            }
        }

        private void SetControlsVisibility(bool newVisibility)
        {
            BackImage.Visible = !newVisibility;
            lblKicsi.Visible = newVisibility;
            lblNagy.Visible = newVisibility;
            DynamicContentContainer.Visible = newVisibility;
            btnSzamol.Visible = newVisibility;
            btnTorol.Visible = newVisibility;
            lblFizetendo.Visible = newVisibility;
            txtFizetendo.Visible = newVisibility;
            btnAdatBe.Visible = !newVisibility;
        }
    }
}
