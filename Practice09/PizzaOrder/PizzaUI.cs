﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PizzaOrder
{
    class PizzaUI
    {
        private const int nameX = 21;
        private const int nameY = 12;

        private const int panelX = 187;
        private const int panelY = 10;
        private const int panelW = 221;
        private const int panelH = 26;

        private const int price1X = 16;
        private const int price2X = 104;
        private const int priceY = 3;
        private const int priceW = 80;
        private const int priceH = 21;

        private const int pieceX = 447;
        private const int pieceY = 11;
        private const int pieceW = 34;
        private const int pieceH = 22;

        private const int pieceTextX = 490;
        private const int pieceTextY = 14;
        private const int pieceTextW = 40;
        private const int pieceTextH = 17;

        private CheckBox name;
        private RadioButton smallSize;
        private RadioButton bigSize;
        private TextBox pieces;
        private Pizza pizza;

        public PizzaUI(Panel container, int y, Pizza pizza)
        {
            this.pizza = pizza; 

            name = new CheckBox();
            name.Text = pizza.Nev;
            name.Location = new Point(nameX, nameY + y);
            container.Controls.Add(name);

            Panel rbContainer = new Panel();
            rbContainer.Location = new Point(panelX, panelY + y);
            rbContainer.Size = new Size(panelW, panelH);
            container.Controls.Add(rbContainer);

            smallSize = new RadioButton();
            smallSize.Text = pizza.ArKicsi.ToString() + " Ft";
            smallSize.Location = new Point(price1X, priceY);
            smallSize.Size = new Size(priceW, priceH);
            rbContainer.Controls.Add(smallSize);

            bigSize = new RadioButton();
            bigSize.Text = pizza.ArNagy.ToString() + " Ft";
            bigSize.Location = new Point(price2X, priceY);
            bigSize.Size = new Size(priceW, priceH);
            rbContainer.Controls.Add(bigSize);

            pieces = new TextBox();
            pieces.Location = new Point(pieceX, pieceY + y);
            pieces.Size = new Size(pieceW, pieceH);
            container.Controls.Add(pieces);

            Label piecesText = new Label();
            piecesText.Text = "darab";
            piecesText.Location = new Point(pieceTextX, pieceTextY + y);
            piecesText.Size = new Size(pieceTextW, pieceTextH);
            container.Controls.Add(piecesText);
        }

        public void Clear()
        {
            name.Checked = false;
            smallSize.Checked = true;
            pieces.Clear();
        }

        public int GetTotalPrice()
        {
            if (name.Checked)
            {
                int count = int.Parse(pieces.Text);

                if(count <=0)
                {
                    throw new ArgumentOutOfRangeException();
                }

                if (smallSize.Checked)
                {
                    return pizza.ArKicsi * count;
                }
                else
                {
                    return pizza.ArNagy * count;
                }
            }

            return 0;
        }
    }
}
