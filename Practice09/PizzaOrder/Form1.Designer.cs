﻿namespace PizzaOrder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNagy = new System.Windows.Forms.Label();
            this.lblKicsi = new System.Windows.Forms.Label();
            this.btnAdatBe = new System.Windows.Forms.Button();
            this.txtFizetendo = new System.Windows.Forms.TextBox();
            this.lblFizetendo = new System.Windows.Forms.Label();
            this.btnTorol = new System.Windows.Forms.Button();
            this.btnSzamol = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.DynamicContentContainer = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.BackImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.BackImage)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNagy
            // 
            this.lblNagy.AutoSize = true;
            this.lblNagy.Location = new System.Drawing.Point(418, 77);
            this.lblNagy.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNagy.Name = "lblNagy";
            this.lblNagy.Size = new System.Drawing.Size(55, 17);
            this.lblNagy.TabIndex = 38;
            this.lblNagy.Text = "lblNagy";
            // 
            // lblKicsi
            // 
            this.lblKicsi.AutoSize = true;
            this.lblKicsi.Location = new System.Drawing.Point(293, 77);
            this.lblKicsi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKicsi.Name = "lblKicsi";
            this.lblKicsi.Size = new System.Drawing.Size(51, 17);
            this.lblKicsi.TabIndex = 37;
            this.lblKicsi.Text = "lblKicsi";
            // 
            // btnAdatBe
            // 
            this.btnAdatBe.Location = new System.Drawing.Point(309, 355);
            this.btnAdatBe.Margin = new System.Windows.Forms.Padding(4);
            this.btnAdatBe.Name = "btnAdatBe";
            this.btnAdatBe.Size = new System.Drawing.Size(211, 28);
            this.btnAdatBe.TabIndex = 36;
            this.btnAdatBe.Text = "Adatbevitel";
            this.btnAdatBe.UseVisualStyleBackColor = true;
            this.btnAdatBe.Click += new System.EventHandler(this.btnAdatBe_Click);
            // 
            // txtFizetendo
            // 
            this.txtFizetendo.Location = new System.Drawing.Point(369, 298);
            this.txtFizetendo.Margin = new System.Windows.Forms.Padding(4);
            this.txtFizetendo.Name = "txtFizetendo";
            this.txtFizetendo.ReadOnly = true;
            this.txtFizetendo.Size = new System.Drawing.Size(132, 22);
            this.txtFizetendo.TabIndex = 34;
            this.txtFizetendo.TabStop = false;
            this.txtFizetendo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblFizetendo
            // 
            this.lblFizetendo.AutoSize = true;
            this.lblFizetendo.Font = new System.Drawing.Font("Garamond", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblFizetendo.ForeColor = System.Drawing.Color.Maroon;
            this.lblFizetendo.Location = new System.Drawing.Point(232, 298);
            this.lblFizetendo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFizetendo.Name = "lblFizetendo";
            this.lblFizetendo.Size = new System.Drawing.Size(98, 22);
            this.lblFizetendo.TabIndex = 33;
            this.lblFizetendo.Text = "Fizetendő:";
            // 
            // btnTorol
            // 
            this.btnTorol.Location = new System.Drawing.Point(70, 355);
            this.btnTorol.Margin = new System.Windows.Forms.Padding(4);
            this.btnTorol.Name = "btnTorol";
            this.btnTorol.Size = new System.Drawing.Size(107, 31);
            this.btnTorol.TabIndex = 32;
            this.btnTorol.Text = "Töröl";
            this.btnTorol.UseVisualStyleBackColor = true;
            this.btnTorol.Click += new System.EventHandler(this.btnTorol_Click);
            // 
            // btnSzamol
            // 
            this.btnSzamol.Location = new System.Drawing.Point(70, 294);
            this.btnSzamol.Margin = new System.Windows.Forms.Padding(4);
            this.btnSzamol.Name = "btnSzamol";
            this.btnSzamol.Size = new System.Drawing.Size(107, 31);
            this.btnSzamol.TabIndex = 31;
            this.btnSzamol.Text = "Számol";
            this.btnSzamol.UseVisualStyleBackColor = true;
            this.btnSzamol.Click += new System.EventHandler(this.btnSzamol_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // DynamicContentContainer
            // 
            this.DynamicContentContainer.AutoScroll = true;
            this.DynamicContentContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DynamicContentContainer.Location = new System.Drawing.Point(12, 110);
            this.DynamicContentContainer.Name = "DynamicContentContainer";
            this.DynamicContentContainer.Size = new System.Drawing.Size(800, 166);
            this.DynamicContentContainer.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(804, 50);
            this.label1.TabIndex = 40;
            this.label1.Text = "Pizza rendelés";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BackImage
            // 
            this.BackImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BackImage.Image = global::PizzaOrder.Properties.Resources.pizza;
            this.BackImage.Location = new System.Drawing.Point(0, 0);
            this.BackImage.Name = "BackImage";
            this.BackImage.Size = new System.Drawing.Size(825, 410);
            this.BackImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BackImage.TabIndex = 41;
            this.BackImage.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 410);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNagy);
            this.Controls.Add(this.lblKicsi);
            this.Controls.Add(this.btnAdatBe);
            this.Controls.Add(this.txtFizetendo);
            this.Controls.Add(this.lblFizetendo);
            this.Controls.Add(this.btnTorol);
            this.Controls.Add(this.btnSzamol);
            this.Controls.Add(this.DynamicContentContainer);
            this.Controls.Add(this.BackImage);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pizza rendelés";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.BackImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNagy;
        private System.Windows.Forms.Label lblKicsi;
        private System.Windows.Forms.Button btnAdatBe;
        private System.Windows.Forms.TextBox txtFizetendo;
        private System.Windows.Forms.Label lblFizetendo;
        private System.Windows.Forms.Button btnTorol;
        private System.Windows.Forms.Button btnSzamol;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel DynamicContentContainer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox BackImage;
    }
}

