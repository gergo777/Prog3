﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MDIApp
{
    public partial class MainForm : Form
    {
        private int docCount = 0;
        private SplashForm splashForm;

        public MainForm()
        {
            InitializeComponent();

            splashForm = new SplashForm();
            splashForm.Show();
            splashForm.Close();
        }

        private void addDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DocumentForm newDocument = new DocumentForm(
                String.Format("A(z) {0}. dokumentum tartalma", docCount)
                );
            if (this.IsMdiContainer)
            {
                newDocument.MdiParent = this;
            }
            docCount++;
            newDocument.Text = String.Format("{0}. dokumentum ablak", docCount);
            newDocument.Show();
        }

        private void mosaicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.Cascade);
        }

        private void horizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void verticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileVertical);
        }

    }
}
