﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Uszoverseny {
    public partial class VersenyForm : Form {


        private DateTime alap = new DateTime(2000, 01, 01, 0, 0, 0);
        private int sorszam = -1;

        private List<Versenyzo> versenyzok;

        public int Tav
        {
            get
            {
                return (int)tav.Value;
            }
        }

        public string Versenyszam
        {
            get
            {
                return versenyszam.Text;
            }
        }


        public VersenyForm(List<Versenyzo> versenyzok) {
            InitializeComponent();

            ido.Value = alap;
            versenyszam.SelectedIndex = 0;

            this.versenyzok = versenyzok;
        }

        private void btnVerseny_Click(object sender, EventArgs e) {
            tav.Enabled = false;
            versenyszam.Enabled = false;
            btnVerseny.Enabled = false;

            ido.Enabled = true;
            btnKovetkezo.Enabled = true;

            sorszam = 0;
            ShowCurrentCompetitor();
        }

        private void btnKovetkezo_Click(object sender, EventArgs e) {
            TimeSpan elapsed = ido.Value - alap;
            versenyzok[sorszam].Versenyez(elapsed);

            if(++sorszam < versenyzok.Count)
            {
                ShowCurrentCompetitor();
            }
            else
            {
                Close();
            }
        }

        private void ShowCurrentCompetitor()
        {
            txtVersenyzo.Text = versenyzok[sorszam].Nev;
            ido.Value = alap;
        }

        private void VersenyForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(sorszam >= 0 && sorszam < versenyzok.Count)
            {
                MessageBox.Show("Adja meg az összes időeredményt!", "Figyelem");
                e.Cancel = true;
            }
        }
    }
}
