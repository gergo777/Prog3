﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Theatre
{
    public partial class TicketForm : Form
    {
        private List<Play> Plays;
        private List<CheckBox> checks = new List<CheckBox>();
        private List<TextBox> counts = new List<TextBox>();

        public TicketForm(List<Play> plays)
        {
            InitializeComponent();

            Plays = plays;
        }

        private void TicketForm_Load(object sender, EventArgs e)
        {
            PictureBox image;
            CheckBox check;
            TextBox count;
            Label pcs;
            int shift = 0;
            foreach(Play play in Plays)
            {
                image = new PictureBox();
                image.Location = new System.Drawing.Point(12, 12 + shift);
                image.Image = Image.FromFile(play.ImageFile + ".jpg");
                image.SizeMode = PictureBoxSizeMode.Zoom;
                image.Size = new System.Drawing.Size(80, 80);
                image.TabStop = false;

                check = new CheckBox();
                check.AutoSize = true;
                check.Location = new System.Drawing.Point(98, 42 + shift);
                check.Size = new System.Drawing.Size(240, 21);
                check.Text = play.Title;
                check.UseVisualStyleBackColor = true;

                check.Enabled = play.HasAvailableTicket;

                count = new TextBox();
                count.Location = new System.Drawing.Point(340, 41 + shift);
                count.Size = new System.Drawing.Size(30, 22);
                count.Enabled = play.HasAvailableTicket;

                pcs = new Label();
                pcs.AutoSize = true;
                pcs.Location = new System.Drawing.Point(380, 44 + shift);
                pcs.Size = new System.Drawing.Size(24, 17);
                pcs.Text = "db";

                shift += 92;

                ContentContainer.Controls.Add(image);
                ContentContainer.Controls.Add(check);
                ContentContainer.Controls.Add(count);
                ContentContainer.Controls.Add(pcs);

                checks.Add(check);
                counts.Add(count);
            }
        }

        private void Order_Click(object sender, EventArgs e)
        {
            bool hasSelected = false;
            int count;
            int toPay = 0;
            for (int i = 0; i < checks.Count; i++)
            {
                try
                {
                    if (checks[i].Checked)
                    {
                        hasSelected = true;
                        count = int.Parse(counts[i].Text);
                        Plays[i].OrderTicket(count);
                        toPay += count * Play.TICKET_PRICE;
                        checks[i].Checked = false;
                        counts[i].Text = "";

                        checks[i].Enabled = Plays[i].HasAvailableTicket;
                        counts[i].Enabled = Plays[i].HasAvailableTicket;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                        String.Format(
                            "Hiba a rendelés {0}. sorában!\n{1}",
                            i + 1, ex.Message
                        ),
                        "Rendelési hiba"
                    );
                }
            }
            Payment.Text = toPay.ToString() + " Ft";

            if(!hasSelected)
            {
                MessageBox.Show(
                    "Kérem válassza ki a megvásárolni kívánt jegyeket!",
                    "Figyelem"
                );
            }
        }
    }
}
