﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Theatre
{
    public partial class ListForm : Form
    {
        private List<Play> Plays;

        public ListForm(List<Play> plays)
        {
            InitializeComponent();

            Plays = plays;

            foreach(Play p in Plays)
            {
                PlayList.Items.Add(p);
            }
        }

        private void PlayList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PlayList.SelectedItem != null)
            {
                Play selectedItem = PlayList.SelectedItem as Play;
                HeadImage.Image = Image.FromFile(selectedItem.ImageFile + ".jpg");
                Title.Text = selectedItem.Title;

                AuthorTitle.Text = selectedItem.AuthorTitle + ":";
                Author.Text = selectedItem.Author;
                Actors.Text = selectedItem.Actors;
            }
            else
            {
                HeadImage.Image = null;
                Title.Text = "";
                AuthorTitle.Text = "";
                Author.Text = "";
                Actors.Text = "";
            }
        }
    }
}
