﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Theatre
{
    public partial class MainForm : Form
    {
        private List<Play> Plays = new List<Play>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            műsorToolStripMenuItem.Enabled = false;
            jegyrendelésToolStripMenuItem.Enabled = false;

            openFileDialog1.InitialDirectory = Directory.GetCurrentDirectory();
            openFileDialog1.FileName = "szinhaz.txt";
            openFileDialog1.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
        }

        private void megnyitásToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamReader reader = null;

                try
                {
                    reader = new StreamReader(openFileDialog1.FileName);
                    string[] words1, words2;
                    while(!reader.EndOfStream)
                    {
                        words1 = reader.ReadLine().Split(';');
                        words2 = reader.ReadLine().Split(':');

                        Plays.Add(
                            new Play(
                                words1[0],
                                words1[1],
                                int.Parse(words1[2].Trim()),
                                words2[0].Trim(),
                                words2[1].Trim(),
                                reader.ReadLine().Replace(";", Environment.NewLine)
                            )
                        );
                    }

                    megnyitásToolStripMenuItem.Enabled = false;
                    műsorToolStripMenuItem.Enabled = true;
                    jegyrendelésToolStripMenuItem.Enabled = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                        "Hiba a fájl olvasása közben!" + Environment.NewLine + ex.Message, 
                        "Hiba!"
                    );
                }
                finally
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }
                }
            }
        }

        private void kilépésToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void műsorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListForm listForm = new ListForm(Plays);

            listForm.ShowDialog();
        }

        private void jegyrendelésToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TicketForm ticketForm = new TicketForm(Plays);

            ticketForm.ShowDialog();
        }

        private void névjegyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Gipsz Jakab (GIJXYZ)", "Névjegy");
        }

        private DialogResult AskExitConfirm()
        {
            return
                MessageBox.Show(
                    "Valóban ki akar lépni?",
                    "Kilépés",
                    MessageBoxButtons.YesNo
                );
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = AskExitConfirm() != DialogResult.Yes;
        }
    }
}
