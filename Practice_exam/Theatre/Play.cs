﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Theatre
{
    public class Play
    {
        public const int TICKET_PRICE = 1300;


        public String Title { get; private set; }
        public String ImageFile { get; private set; }
        public int TicketAvailable { get; private set; }
        public int TicketSold { get; set; }
        public String AuthorTitle { get; private set; }
        public String Author { get; private set; }
        public String Actors { get; private set; }

        public bool HasAvailableTicket
        {
            get
            {
                return TicketAvailable > 0;
            }
        }

        public Play(String title, String imageFile, int available, 
            String authorTitle, String author, 
            String actors)
        {
            Title = title;
            ImageFile = imageFile;
            TicketAvailable = available;
            TicketSold = 0;
            AuthorTitle = authorTitle;
            Author = author;
            Actors = actors;
        }

        public void OrderTicket(int count)
        {
            if (count <= 0 || count > TicketAvailable)
                throw new ArgumentOutOfRangeException();

            TicketAvailable -= count;
            TicketSold += count;
        }

        public override string ToString()
        {
            return Title;
        }
    }
}
