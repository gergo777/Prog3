﻿namespace Theatre
{
    partial class ListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PlayList = new System.Windows.Forms.ListBox();
            this.HeadImage = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Title = new System.Windows.Forms.TextBox();
            this.Author = new System.Windows.Forms.TextBox();
            this.AuthorTitle = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Actors = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.HeadImage)).BeginInit();
            this.SuspendLayout();
            // 
            // PlayList
            // 
            this.PlayList.FormattingEnabled = true;
            this.PlayList.ItemHeight = 16;
            this.PlayList.Location = new System.Drawing.Point(12, 31);
            this.PlayList.Name = "PlayList";
            this.PlayList.Size = new System.Drawing.Size(257, 308);
            this.PlayList.Sorted = true;
            this.PlayList.TabIndex = 0;
            this.PlayList.SelectedIndexChanged += new System.EventHandler(this.PlayList_SelectedIndexChanged);
            // 
            // HeadImage
            // 
            this.HeadImage.Location = new System.Drawing.Point(278, 51);
            this.HeadImage.Name = "HeadImage";
            this.HeadImage.Size = new System.Drawing.Size(200, 200);
            this.HeadImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.HeadImage.TabIndex = 1;
            this.HeadImage.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Előadások:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(275, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Kép";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(275, 254);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Cím:";
            // 
            // Title
            // 
            this.Title.Location = new System.Drawing.Point(278, 274);
            this.Title.Name = "Title";
            this.Title.ReadOnly = true;
            this.Title.Size = new System.Drawing.Size(200, 22);
            this.Title.TabIndex = 5;
            // 
            // Author
            // 
            this.Author.Location = new System.Drawing.Point(278, 319);
            this.Author.Name = "Author";
            this.Author.ReadOnly = true;
            this.Author.Size = new System.Drawing.Size(200, 22);
            this.Author.TabIndex = 7;
            // 
            // AuthorTitle
            // 
            this.AuthorTitle.AutoSize = true;
            this.AuthorTitle.Location = new System.Drawing.Point(275, 299);
            this.AuthorTitle.Name = "AuthorTitle";
            this.AuthorTitle.Size = new System.Drawing.Size(0, 17);
            this.AuthorTitle.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(501, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Szereposztás:";
            // 
            // Actors
            // 
            this.Actors.Location = new System.Drawing.Point(504, 51);
            this.Actors.Name = "Actors";
            this.Actors.ReadOnly = true;
            this.Actors.Size = new System.Drawing.Size(284, 290);
            this.Actors.TabIndex = 9;
            this.Actors.Text = "";
            // 
            // ListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 363);
            this.Controls.Add(this.Actors);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Author);
            this.Controls.Add(this.AuthorTitle);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.HeadImage);
            this.Controls.Add(this.PlayList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ListForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Műsor";
            ((System.ComponentModel.ISupportInitialize)(this.HeadImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox PlayList;
        private System.Windows.Forms.PictureBox HeadImage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Title;
        private System.Windows.Forms.TextBox Author;
        private System.Windows.Forms.Label AuthorTitle;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox Actors;
    }
}