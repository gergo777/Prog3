﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempStat
{
    class Day
    {
        public int MinTemp { get; private set; }
        public int MaxTemp { get; private set; }

        public int AvgTemp
        {
            get
            {
                return (MaxTemp + MinTemp)/ 2;
            }
        }

        public Day(int minTemp, int maxTemp)
        {
            MinTemp = minTemp;
            MaxTemp = maxTemp;
        }


    }
}
