﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempStat
{
    class Program
    {
        private const int DAY_COUNT = 4;

        static void Main(string[] args)
        {
            List<Day> days = new List<Day>();

            int sumOfAvgs = 0;
            for (int i = 0; i < DAY_COUNT; i++)
            {
                days.Add(
                    GetDayFromConsole()
                );

                sumOfAvgs += days[i].AvgTemp;
            }

            Console.WriteLine("Átlaghőmérséklet: {0}", 
                sumOfAvgs / days.Count
            );
        }

        private static Day GetDayFromConsole()
        {
            Console.WriteLine("{0}. nap", i + 1);
            Console.Write("Minimális hőmérséklet: ");
            string minText = Console.ReadLine();
            Console.Write("Maximális hőmérséklet: ");
            string maxText = Console.ReadLine();

            return new Day(
                    Convert.ToInt32(minText),
                    Convert.ToInt32(maxText)
                )

        }
    }
}
