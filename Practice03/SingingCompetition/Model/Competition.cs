﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingingCompetition.Model
{
    class Competition
    {
        public int JuryCount { get; private set; }
        public int PointsMin { get; private set; }
        public int PointsMax { get; private set; }

        private List<Competitor> competitors = new List<Competitor>();
        private Random random = new Random();

        public Competition(int juryCount, int pointsMin, int pointsmax)
        {
            JuryCount = juryCount;
            PointsMin = pointsMin;
            PointsMax = pointsmax;
        }

        public void Prepare(string fileName)
        {
            StreamReader reader = new StreamReader(fileName);

            while(!reader.EndOfStream)
            {
                competitors.Add(
                    new Competitor(
                        competitors.Count + 1,
                        reader.ReadLine(),
                        reader.ReadLine()
                    )
                );
            }

            reader.Close();
        }

        public void ListCompetitors()
        {
            foreach(Competitor competitor in competitors)
            {
                Console.WriteLine(competitor);
            }
        }

        public void EvaluateCompetitors()
        {
            int newPoints;
            foreach (Competitor competitor in competitors)
            {
                for (int jury = 1; jury <= JuryCount; jury++)
                {
                    newPoints = random.Next(PointsMin, PointsMax);
                    Console.WriteLine(
                        "A {0} sorszámú versenyző az {1}. zsűritagtól {2} pontot kapott",
                        competitor.Id, jury, newPoints
                    );
                    competitor.AddPoints(newPoints);
                }
            }
        }

        public Competitor GetOneWinner()
        {
            Competitor winner = null;
            foreach(Competitor competitor in competitors)
            {
                if(winner == null 
                    || winner.Points < competitor.Points)
                {
                    winner = competitor;
                }
            }

            return winner;
        }

        public void SortByPoints()
        {
            competitors.Sort();
        }

        public void ListWinners()
        {
            SortByPoints();

            Competitor winner = competitors[0];
            int idx = 0;
            Console.WriteLine("A győztesek:");
            while (competitors[idx].Points == winner.Points)
            {
                Console.WriteLine("\t{0}", competitors[idx++]);
            }
        }

        public void SortById()
        {
            competitors.Sort((x, y) => x.Id.CompareTo(y.Id));
        }

        public Competitor Find(int id)
        {
            SortById();

            return competitors.Find(x => x.Id == id);
        }
    }
}
