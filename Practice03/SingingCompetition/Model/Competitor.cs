﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingingCompetition.Model
{
    class Competitor : IComparable
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public string Specialization { get; private set; }

        public int Points { get; private set; }

        public Competitor(int id, string name, string spec)
        {
            Id = id;
            Name = name;
            Specialization = spec;
        }

        public void AddPoints(int newPoints)
        {
            Points += newPoints;
        }

        public override string ToString()
        {
            return String.Format("#{0} sorszámú {1} versenyző pointjai: {2}", Id, Name, Points);
        }

        public int CompareTo(object obj)
        {
            Competitor other = (Competitor)obj;

            return Points < other.Points ? 1 : (Points > other.Points ? -1 : 0);
        }
    }
}
