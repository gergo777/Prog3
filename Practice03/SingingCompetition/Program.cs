﻿using SingingCompetition.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingingCompetition
{
    class Program
    {
        private const int JURY_COUNT = 5;
        private const int POINTS_MIN = 1;
        private const int POINTS_MAX = 10;

        private static Competition competition;

        static void Main(string[] args)
        {
            competition = new Competition(JURY_COUNT, POINTS_MIN, POINTS_MAX);

            competition.Prepare("versenyzok.txt");
            competition.ListCompetitors();

            Halt();

            competition.EvaluateCompetitors();

            Halt();

            competition.ListCompetitors();

            Halt();

            Console.WriteLine("A győztes: {0}", competition.GetOneWinner());

            Halt();

            competition.ListWinners();

            Halt();

            competition.SortById();
            competition.ListCompetitors();

            bool continueSearch;
            do
            {
                continueSearch = FindCompetitor();
            }
            while (continueSearch);
        }

        static void Halt()
        {
            Console.Write("Nyomjon le egy billentyűt! ");
            Console.ReadKey();
            Console.WriteLine();
        }

        static bool FindCompetitor()
        {
            Console.WriteLine("A keresett rajtszám (kilépés = -1): ");
            int id;
            while(!int.TryParse(Console.ReadLine(), out id))
            {
                Console.WriteLine("A keresett rajtSZÁM (egész): ");
            }

            if (id == -1)
            {
                return false;
            }

            Competitor found = competition.Find(id);

            if(found == null)
            {
                Console.WriteLine("Sajnos nincs ilyen versenyző!");
            }
            else
            {
                Console.WriteLine("A talált versenyző: {0}", found);
            }

            return true;
        }
    }
}
