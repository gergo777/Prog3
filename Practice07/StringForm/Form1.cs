﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StringForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Greetings_Click(object sender, EventArgs e)
        {
            int spacePos = FullName.Text.IndexOf(' ');
            String VezetekNev = FullName.Text.Substring(0, spacePos - 1);
            String KeresztNev = FullName.Text.Substring(spacePos + 1);

            string[] names = FullName.Text.Split(' ');

            if (names.Length == 2)
            {
                LastName.Text = names[0].ToUpper();
                FirstName.Text = names[1].ToLower();
                NameLength.Text = FullName.Text.Length.ToString();

                Message.Text = "Hello " + names[1];
            }
            else
            {
                MessageBox.Show(
                    "A vezetéknevet és a keresztnevet egy SPACE válassza el!", 
                    "Figyelem!");
            }
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            FullName.Clear();
            LastName.Clear();
            FirstName.Clear();
            NameLength.Clear();
            Message.Text = "";
        }

        private void Doctor_Click(object sender, EventArgs e)
        {
            if (
                !LastName.Text.StartsWith("dr.")
                && LastName.Text.Length > 0
                )
            {
                LastName.Text = "dr. " + LastName.Text;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Message.Text = "";
        }
    }
}
