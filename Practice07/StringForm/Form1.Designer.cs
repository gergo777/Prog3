﻿namespace StringForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.FullName = new System.Windows.Forms.TextBox();
            this.LastName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.FirstName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.NameLength = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Message = new System.Windows.Forms.Label();
            this.Greetings = new System.Windows.Forms.Button();
            this.Doctor = new System.Windows.Forms.Button();
            this.Clear = new System.Windows.Forms.Button();
            this.Close = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Teljes név:";
            // 
            // FullName
            // 
            this.FullName.Location = new System.Drawing.Point(124, 12);
            this.FullName.Name = "FullName";
            this.FullName.Size = new System.Drawing.Size(236, 22);
            this.FullName.TabIndex = 0;
            // 
            // LastName
            // 
            this.LastName.Location = new System.Drawing.Point(124, 40);
            this.LastName.Name = "LastName";
            this.LastName.ReadOnly = true;
            this.LastName.Size = new System.Drawing.Size(236, 22);
            this.LastName.TabIndex = 3;
            this.LastName.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Vezetéknév:";
            // 
            // FirstName
            // 
            this.FirstName.Location = new System.Drawing.Point(124, 68);
            this.FirstName.Name = "FirstName";
            this.FirstName.ReadOnly = true;
            this.FirstName.Size = new System.Drawing.Size(236, 22);
            this.FirstName.TabIndex = 5;
            this.FirstName.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Keresztnév:";
            // 
            // NameLength
            // 
            this.NameLength.Location = new System.Drawing.Point(124, 96);
            this.NameLength.Name = "NameLength";
            this.NameLength.ReadOnly = true;
            this.NameLength.Size = new System.Drawing.Size(236, 22);
            this.NameLength.TabIndex = 7;
            this.NameLength.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Névhossz:";
            // 
            // Message
            // 
            this.Message.AutoSize = true;
            this.Message.Font = new System.Drawing.Font("Segoe Script", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Message.Location = new System.Drawing.Point(35, 160);
            this.Message.Name = "Message";
            this.Message.Size = new System.Drawing.Size(139, 57);
            this.Message.TabIndex = 8;
            this.Message.Text = "label5";
            // 
            // Greetings
            // 
            this.Greetings.Location = new System.Drawing.Point(35, 245);
            this.Greetings.Name = "Greetings";
            this.Greetings.Size = new System.Drawing.Size(97, 23);
            this.Greetings.TabIndex = 1;
            this.Greetings.Text = "Üdvözlés";
            this.Greetings.UseVisualStyleBackColor = true;
            this.Greetings.Click += new System.EventHandler(this.Greetings_Click);
            // 
            // Doctor
            // 
            this.Doctor.Location = new System.Drawing.Point(267, 245);
            this.Doctor.Name = "Doctor";
            this.Doctor.Size = new System.Drawing.Size(93, 23);
            this.Doctor.TabIndex = 2;
            this.Doctor.Text = "Doktor";
            this.Doctor.UseVisualStyleBackColor = true;
            this.Doctor.Click += new System.EventHandler(this.Doctor_Click);
            // 
            // Clear
            // 
            this.Clear.Location = new System.Drawing.Point(35, 296);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(97, 23);
            this.Clear.TabIndex = 3;
            this.Clear.Text = "Törlés";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // Close
            // 
            this.Close.Location = new System.Drawing.Point(267, 296);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(93, 23);
            this.Close.TabIndex = 4;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 349);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.Clear);
            this.Controls.Add(this.Doctor);
            this.Controls.Add(this.Greetings);
            this.Controls.Add(this.Message);
            this.Controls.Add(this.NameLength);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.FirstName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LastName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.FullName);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FullName;
        private System.Windows.Forms.TextBox LastName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox FirstName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox NameLength;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Message;
        private System.Windows.Forms.Button Greetings;
        private System.Windows.Forms.Button Doctor;
        private System.Windows.Forms.Button Clear;
        private System.Windows.Forms.Button Close;
    }
}

