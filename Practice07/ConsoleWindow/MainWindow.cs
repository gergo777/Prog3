﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleWindow
{
    class MainWindow : Form
    {
        private ListBox listBox1;
        private CheckedListBox checkedListBox1;
        private ComboBox comboBox1;
        private ComboBox comboBox2;
        Label message;

        public MainWindow()
        {
            Initialize();
        }

        private void Initialize()
        {
            Size = new Size(400, 300);
            Text = "My first windows form";
            BackColor = Color.Blue;
            CenterToScreen();

            Button button = new Button();
            button.Location = new Point(30, 30);
            button.Text = "Nyomd meg";
            button.BackColor = Color.Yellow;
            button.Click += new EventHandler(Button_Click);

            message = new Label();
            message.Text = "Üres";
            message.Location = new Point(30, 60);
            message.ForeColor = Color.White;

            Controls.Add(button);
            Controls.Add(message);
        }

        private void Button_Click(object sender, EventArgs args)
        {
            message.Text = "Hello";
        }
    }
}
