﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FirstWindowsForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void GreetingsButton_Click(object sender, EventArgs e)
        {
            SayHello();
        }

        private void SayHello()
        {
            if (LastName.Text.Length > 0
                && FirstName.Text.Length > 0
                && PhoneNumber.MaskCompleted)
            {
                Message.Text = "Szép napot " + LastName.Text
                    + " " + FirstName.Text
                    + Environment.NewLine + "Tel: " + PhoneNumber.Text;
            }
            else
            {
                MessageBox.Show(
                    "Kérem adjon meg vezetéknevet, keresztnevet, telefonszámot is!",
                    "Figyelem!"
                );
            }
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                SayHello();
            }
        }
    }
}
