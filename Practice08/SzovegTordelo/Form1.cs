﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SzovegTordelo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Brake_Click(object sender, EventArgs e)
        {
            BrakeIntoWords();
        }

        private void BrakeIntoWords()
        {
            String[] words = Sentence.Text.Split(' ');
            foreach (String word in words)
            {
                Words.Text += word + Environment.NewLine;
            }
        }

        private void Sentence_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                BrakeIntoWords();
            }
        }
    }
}
