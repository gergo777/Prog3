﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Finish_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            Op1.Clear();
            Op2.Clear();
            Result.Clear();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            try
            {
                Result.Text = (int.Parse(Op1.Text) + int.Parse(Op2.Text)).ToString();
            }
            catch(System.FormatException fe)
            {
                MessageBox.Show(fe.Message, "Hiba!");
            }
        }

        private void Sub_Click(object sender, EventArgs e)
        {
            try
            {
                Result.Text = (int.Parse(Op1.Text) - int.Parse(Op2.Text)).ToString();
            }
            catch (System.FormatException fe)
            {
                MessageBox.Show(fe.Message, "Hiba!");
            }
        }

        private void Mul_Click(object sender, EventArgs e)
        {
            try
            {
                Result.Text = (int.Parse(Op1.Text) * int.Parse(Op2.Text)).ToString();
            }
            catch (System.FormatException fe)
            {
                MessageBox.Show(fe.Message, "Hiba!");
            }
        }

        private void DivInt_Click(object sender, EventArgs e)
        {
            try
            {
                Result.Text = (int.Parse(Op1.Text) / int.Parse(Op2.Text)).ToString();
            }
            catch (System.FormatException fe)
            {
                MessageBox.Show(fe.Message, "Hiba!");
            }
        }

        private void DivRem_Click(object sender, EventArgs e)
        {
            try
            {
                Result.Text = (int.Parse(Op1.Text) % int.Parse(Op2.Text)).ToString();
            }
            catch (System.FormatException fe)
            {
                MessageBox.Show(fe.Message, "Hiba!");
            }
        }
    }
}
