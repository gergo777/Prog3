﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HandleDateTime
{
    public partial class MainForm : Form
    {
        System.Windows.Forms.Timer dateRefreshTimer = new System.Windows.Forms.Timer();

        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("hu-HU");

            CurrentDateTime.Text = DateTime.Now.ToString("F");
            Message.Text = "";
            DayOfYear.Text = "";
            EndDate.Text = "";

            dateRefreshTimer.Interval = 1000;
            dateRefreshTimer.Tick += DateRefreshTimer_Tick;
            dateRefreshTimer.Enabled = true;
        }

        private void DateRefreshTimer_Tick(object sender, EventArgs e)
        {
            CurrentDateTime.Text = DateTime.Now.ToString("F");
        }

        private void BirthDate_Leave(object sender, EventArgs e)
        {
            DateTime date;
            try
            {
                date = DateTime.Parse(BirthDate.Text);
                Age.Text = (DateTime.Now.Year - date.Year).ToString();
                BirthDayOfWeek.Text = CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(date.DayOfWeek);

                if(DateTime.Now.Month == date.Month && DateTime.Now.Day == date.Day)
                {
                    Message.Text = "Boldog szülinapot!";
                }
                else {
                    Message.Text = "";
                }
            }
            catch(FormatException) {
                MessageBox.Show("Hibás dátum");
                BirthDate.Focus();
            }
        }

        private void customDate_ValueChanged(object sender, EventArgs e)
        {
            CalcEndDate();
        }

        private void Interval_TextChanged(object sender, EventArgs e)
        {
            CalcEndDate();
        }

        private void CalcEndDate()
        {
            try
            {
                int days = int.Parse(Interval.Text);
                DayOfYear.Text = customDate.Value.DayOfYear.ToString();
                EndDate.Text = customDate.Value.AddDays(days).ToString("D");
            }
            catch (FormatException)
            {
                EndDate.Text = "";
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult response = MessageBox.Show(
                "Biztosan ki akar lépni?", 
                "Figyelem!", 
                MessageBoxButtons.YesNo
            );

            e.Cancel = response != DialogResult.Yes;
        }
    }
}
