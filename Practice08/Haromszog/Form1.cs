﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Haromszog
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Compute_Click(object sender, EventArgs e)
        {
            try
            {
                double a = double.Parse(A.Text);
                double b = double.Parse(B.Text);

                if (a > 0 && b > 0)
                {
                    C.Text = Math.Sqrt(a * a + b * b).ToString();
                }
                else
                {
                    C.Clear();
                    MessageBox.Show("Nem háromszög adatai", "Figyelem!");
                }
            }
            catch(System.FormatException fe)
            {
                MessageBox.Show(fe.Message, "Hiba!");
            }
        }

        private void Finish_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            A.Clear();
            B.Clear();
            C.Clear();
        }
    }
}
