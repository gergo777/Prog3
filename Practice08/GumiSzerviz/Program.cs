﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GumiSzerviz
{
    class Program
    {
        static void Main(string[] args)
        {
            Szerviz szerviz = new Szerviz();

            szerviz.NapKezdes();
            szerviz.MunkaFelvetel();

            szerviz.LeszereltTeliGumik();
            Console.WriteLine(
                String.Format("Átlagos vizsgált állapot: {0}", 
                    szerviz.AtlagosAllapot()
                )
            );
            szerviz.LegjobbGumik();
        }
    }
}
