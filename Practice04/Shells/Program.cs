﻿using Shells.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shells
{
    class Program
    {
        static void Main(string[] args)
        {
            Cylinder c1 = new Cylinder(9, 6);
            Rod r1 = new Rod(10, 3, 5);
            Tube t1 = new Tube(10, 4, 4, 6);
            Tube t2 = new Tube(5, 9, 12, 3);

            Console.WriteLine(c1);
            Console.WriteLine(r1);
            Console.WriteLine(t1);
            Console.WriteLine(t2);
        }
    }
}
