﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shells.Model
{
    class Cylinder
    {
        public int Radius { get; set; }
        public int Height { get; set; }

        virtual public double Volume
        {
            get
            {
                return Radius * 2 * Math.PI * Height;
            }
        }

        public Cylinder(int r, int h)
        {
            this.Radius = r;
            this.Height = h;

        }

        public override string ToString()
        {
            return String.Format("A henger sugara : {0} , a magassága: {1}, térfogata: {2:0.00}", 
                Radius, Height, Volume
            ) ;
        }
    }
}
