﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shells.Model
{
    class Rod : Cylinder
    {
        public int Density { get; set; }

        virtual public double Mass
        {
            get
            {
                if (Density > 0)
                {
                    return Volume * Density;
                }
                else return 0;
            }
        }

        public Rod(int r, int h, int d) : base(r, h)
        {
            this.Density = d;
        }

        public override string ToString()
        {
            return String.Format("A rúd sugara : {0} , a magassága: {1}, térfogata: {2:0.00}, súlya: {3:0.00}", 
                Radius, Height, Volume, Mass
            );
        }
    }
    
}
