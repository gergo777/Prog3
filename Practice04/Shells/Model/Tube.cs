﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shells.Model
{
    class Tube : Rod
    {
        public int Thickness { get; set; }

        public override double Volume
        {
            get
            {
                if (Thickness > 0)
                {
                    return Math.Sqrt(Radius - Thickness) * Math.PI * Height;
                }
                else return 0;
            }
        }

        public Tube(int r, int h, int d, int t) : base(r, h, d)
        {
            this.Thickness = t;
        }

        public string ToString()
        {
            return String.Format("A cső sugara : {0} , a magassága: {1}, térfogata: {2:0.00}, súlya: {3:0.00}", 
                Radius, Height, Volume, Mass
            );
        }
    }
}
