﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatDogContest.Model
{
    class Animal : IComparable
    {
        static bool isInitialized = false;
        static int maxAge;
        static int currentYear;

        static public void Init(int maxAge, int currentYear)
        {
            Animal.maxAge = maxAge;
            Animal.currentYear = currentYear;
            isInitialized = true;
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
        public int BirthYear { get; private set; }
        public int BeautyPoints { get; private set; }
        public int BehaviourPoints { get; private set; }
        private bool isEvaluated;

        public int Age
        {
            get
            {
                return currentYear - BirthYear;
            }
        }

        virtual public int TotalPoints {
            get
            {
                int points;
                if (Age > maxAge)
                {
                    points = 0;
                }
                else
                {
                    points = (maxAge - Age) * BeautyPoints
                        + Age * BehaviourPoints;
                }

                return points;
            }
        }

        public Animal(int id, string name, int birthYear)
        {
            if (!isInitialized)
                throw new Exception("Please Init class first!");

            Id = id;
            Name = name;
            BirthYear = birthYear;
            isEvaluated = false;
        }

        public void Evaluation(int beautyPoint, int behaviourPoint)
        {
            BeautyPoints = beautyPoint;
            BehaviourPoints = behaviourPoint;
            isEvaluated = true;
        }

        public override string ToString()
        {
            string serializedOutput;
            if (!isEvaluated)
            {
                serializedOutput = String.Format("{0}(#{1}) még nincs pontozva", Name, Id);
            }
            else
            {
                serializedOutput = String.Format("{0}(#{1}) állat pontszáma: {2}",
                    Name, Id, TotalPoints
                    );
            }

            return serializedOutput;
        }

        #region IComparable interface implementation

        public int CompareTo(object obj)
        {
            Animal other = (Animal)obj;

            return TotalPoints < other.TotalPoints ? 1 : (TotalPoints > other.TotalPoints ? -1 : 0);
        }

        #endregion
    }
}
