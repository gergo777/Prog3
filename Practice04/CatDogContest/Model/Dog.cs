﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatDogContest.Model
{
    class Dog : Animal
    {
        public int RelationToKeeper { get; private set; }

        public Dog(int id, string name, int birthYear, 
            int relationToKeeper) 
            : base(id, name, birthYear)
        {
            RelationToKeeper = relationToKeeper;
        }

        override public int TotalPoints
        {
            get
            {
                return base.TotalPoints + RelationToKeeper;
            }
        }

        public override string ToString()
        {
            return String.Format("A {0} nevű, #{1} rajtszámú kutya pontszáma: {2}", Name, Id, TotalPoints);
        }
    }
}
