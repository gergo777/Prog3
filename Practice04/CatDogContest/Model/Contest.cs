﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatDogContest.Model
{
    class Contest
    {
        public int PointsMin { get; private set; }
        public int PointsMax { get; private set; }

        public int MaxAge { get; private set; }
        public int CurrentYear { get; private set; }

        private List<Animal> competitors = new List<Animal>();
        private Random random = new Random();

        public Contest(int pointsMin, int pointsMax, int maxAge, int currentYear)
        {
            PointsMin = pointsMin;
            PointsMax = pointsMax;
            MaxAge = maxAge;
            CurrentYear = currentYear;
        }

        public void Registration(string fileName)
        {
            StreamReader reader = new StreamReader(fileName);

            string race;

            while(!reader.EndOfStream)
            {
                race = reader.ReadLine();

                if (race == "kutya")
                {
                    competitors.Add(
                        new Dog(
                            competitors.Count + 1,
                            reader.ReadLine(),
                            Convert.ToInt32(reader.ReadLine()),
                            random.Next(PointsMin, PointsMax+1)
                        )
                    );
                }
                else
                {
                    competitors.Add(
                        new Cat(
                            competitors.Count + 1,
                            reader.ReadLine(),
                            Convert.ToInt32(reader.ReadLine()),
                            reader.ReadLine() == "true"
                        )
                    );
                }
            }

            reader.Close();
        }

        public void EvaluateCompetitors()
        {
            int beautyPoints, behaviourPoints;
            foreach (Animal competitor in competitors)
            {
                beautyPoints = random.Next(PointsMin, PointsMax+1);
                behaviourPoints = random.Next(PointsMin, PointsMax+1);
                Console.WriteLine(
                        "A #{0} rajtszámú versenyző értékelése: Szépség={1} Viselekdés={2}",
                        competitor.Id, beautyPoints, behaviourPoints
                    );
                competitor.Evaluation(beautyPoints, behaviourPoints);
            }
        }

        #region List filters

        public Animal GetOneWinner()
        {
            Animal winner = null;
            foreach(Animal competitor in competitors)
            {
                if(winner == null 
                    || winner.TotalPoints < competitor.TotalPoints)
                {
                    winner = competitor;
                }
            }

            return winner;
        }

        public List<Animal> GetListOfWinners()
        {
            List<Animal> winners = new List<Animal>();
            foreach (Animal competitor in competitors)
            {
                if (winners[0].TotalPoints < competitor.TotalPoints)
                {
                    winners.Clear();
                }

                if (winners.Count == 0 || winners[0].TotalPoints == competitor.TotalPoints)
                {
                    winners.Add(competitor);
                }
            }

            return winners;
        }

        public Animal Find(int id)
        {
            SortById();

            return competitors.Find(x => x.Id == id);
        }

        #endregion

        #region List printers

        public void ListCompetitors()
        {
            foreach (Animal competitor in competitors)
            {
                Console.WriteLine(competitor);
            }
        }

        public void ListAllWinners()
        {
            SortByPoints();

            Animal winner = competitors[0];
            int idx = 0;
            Console.WriteLine("A győztesek:");
            while (competitors[idx].TotalPoints == winner.TotalPoints)
            {
                Console.WriteLine("\t{0}", competitors[idx++]);
            }
        }

        #endregion

        #region Sort

        public void SortByPoints()
        {
            competitors.Sort();
        }

        public void SortById()
        {
            competitors.Sort((x, y) => x.Id.CompareTo(y.Id));
        }

        #endregion

        
    }
}
