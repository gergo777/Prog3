﻿using CatDogContest.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatDogContest
{
    class Program
    {
        private const int POINTS_MIN = 1;
        private const int POINTS_MAX = 10;

        private const int MAX_AGE = 10;
        private const int CURRENT_YEAR = 2018;

        static void Main(string[] args)
        {
            Animal.Init(MAX_AGE, CURRENT_YEAR);

            Contest contest = new Contest(POINTS_MIN, POINTS_MAX, MAX_AGE, CURRENT_YEAR);

            Console.WriteLine("----==== Regisztráció ====----");
            contest.Registration("allatok.txt");
            contest.ListCompetitors();
            Console.WriteLine("----==== Értékelés ====----");
            contest.EvaluateCompetitors();
            Console.WriteLine("----==== A verseny állása ====----");
            contest.ListCompetitors();
            Console.WriteLine("----==== A verseny végeredménye ====----");
            contest.SortByPoints();
            contest.ListCompetitors();
        }
    }
}
