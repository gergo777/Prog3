﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples
{
    class Person
    {
        static public int MaxHeight { get; set; }

        public string Name { get; private set; }
        public int Height { get; private set; }
        public int JumpHeight { get; set; }

        public int Score
        {
            get
            {
                return (Person.MaxHeight - this.Height) * this.JumpHeight;
            }
        }

        public Person(string Name, int Height)
        {
            this.Name = Name;
            this.Height = Height;
        }

        public void TeachTactics()
        {

        }

        public override string ToString()
        {
            return String.Format("{0} pontszáma: {0}", Name, Score);
        }
    }
}
