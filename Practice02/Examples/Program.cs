﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples
{
    class Program
    {
        static void Main(string[] args)
        {
            int c = Program.Calc(2, 3);

            Person.MaxHeight = 250;

            Person player = new Person("Béla", 182);

            player.JumpHeight = 85;
            player.TeachTactics();
        }

        static int Calc(int a, int b)
        {
            return a + b;
        }
    }
}
