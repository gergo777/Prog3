﻿using AnimalContest.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalContest
{
    class Program
    {
        const int YEAR_MIN = 1900;
        const int YEAR_MAX = 2050;

        const int AGE_MIN = 10;
        const int AGE_MAX = 100;

        const int POINT_MIN = 1;
        const int POINT_MAX = 10;

        const int BIRTH_MIN = 1900;
        const int BIRTH_MAX = 2018;

        static Random random = new Random();

        static void Main(string[] args)
        {
            Console.Write("Aktuális év: ");
            int currYear = GetIntFromConsole(YEAR_MIN, YEAR_MAX);
            Console.Write("Maximális versenző életkor: ");
            int maxAge = GetIntFromConsole(AGE_MIN, AGE_MAX);

            Animal.Init(maxAge, currYear);

            Animal animal;
            Animal winner = null;
            ConsoleKeyInfo key;

            do
            {
                animal = GetAnimalFromConsole();
                EvaluateFromConsole(animal);
                Console.WriteLine(animal);

                if (winner == null
                    || winner.TotalPoints < animal.TotalPoints)
                {
                    winner = animal;
                }

                Console.Write("Van még versenyző? (i/n) ");
                key = Console.ReadKey();
                Console.WriteLine();
            } while (char.ToLower(key.KeyChar) == 'i');

            Console.WriteLine("A győztes {0}", winner);
        }

        private static int GetIntFromConsole(int min, int max)
        {
            int number;
            while (!int.TryParse(Console.ReadLine(), out number) || number < min || number > max)
            {
                Console.WriteLine("Kérem írjon egész számot ({0}-{1})!", min, max);
            }
            return number;
        }

        private static Animal GetAnimalFromConsole()
        {
            Console.Write("Az állat neve: ");
            string name = Console.ReadLine();
            Console.Write("Születési éve: ");
            int birthYear = GetIntFromConsole(BIRTH_MIN, BIRTH_MAX);

            return new Animal(name, birthYear); ;
        }

        private static void EvaluateFromConsole(Animal animal)
        {
            Console.WriteLine("Értékelés...");
            Console.Write("Szépség: ");
            int beauty = GetIntFromConsole(POINT_MIN, POINT_MAX);
            Console.Write("Viselkedés: ");
            int behaviour = GetIntFromConsole(POINT_MIN, POINT_MAX);

            animal.Evaluation(beauty, behaviour);
        }

        private static void EvaluateRandomly(Animal animal)
        {
            animal.Evaluation(
                random.Next(POINT_MIN, POINT_MAX + 1),
                random.Next(POINT_MIN, POINT_MAX + 1)
            );
        }
    }
}
