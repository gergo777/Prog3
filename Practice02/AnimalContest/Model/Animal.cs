﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalContest.Model
{
    class Animal
    {
        static bool isInitialized = false;
        static int maxAge;
        static int currentYear;

        static public void Init(int maxAge, int currentYear)
        {
            Animal.maxAge = maxAge;
            Animal.currentYear = currentYear;
            isInitialized = true;
        }

        public string Name { get; private set; }
        public int BirthYear { get; private set; }
        public int BeautyPoints { get; private set; }
        public int BehaviourPoints { get; private set; }
        private bool isEvaluated;

        public int Age
        {
            get
            {
                return currentYear - BirthYear;
            }
        }
        public int TotalPoints {
            get
            {
                if (Age > maxAge) return 0;

                return 
                    (maxAge - Age)*BeautyPoints
                    + Age * BehaviourPoints;
            }
        }

        public Animal(string name, int birthYear)
        {
            if (!isInitialized)
                throw new Exception("Please Init class first!");

            Name = name;
            BirthYear = birthYear;
            isEvaluated = false;
        }

        public void Evaluation(int beautyPoint, int behaviourPoint)
        {
            BeautyPoints = beautyPoint;
            BehaviourPoints = behaviourPoint;
            isEvaluated = true;
        }

        public override string ToString()
        {
            if (!isEvaluated)
                return String.Format("{0} még nincs pontozva", Name);

            return String.Format("{0} állat pontszáma: {1}", 
                Name, TotalPoints
                );
        }
    }
}
